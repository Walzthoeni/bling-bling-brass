/*********
  Rui Santos
  Complete project details at http://randomnerdtutorials.com  
*********/
#include <vector>

#include <Arduino.h>
#include <WiFi.h>

#include <IoAbstraction.h>
#include <FFT.h>
#include <LED-Color-Manager.h>

/**
 * This is the point where you need to define the SSID (name) and 
 * password of the WIFI you are using to communicate with others.
 * 
 * This WIFI can be easily set up by using the hotsport function from 
 * a normal smartphone. (WIFI range is then also limited to the range of the hotspot)
 * 
 * This has to be the same for all band members!
 */

const char *networkName = "SNO";
const char *networkPswd = "strafiato";

/**
 * Define all LED stripes you want to use here
 * 
 * There is a variety of different behavior models.
 * The models are shown in LED-Stripes.h. Here are some examples:
 * 
 * using M = MiddleToOutsideLED<PIN, NUM_LEDS>
 * using E = EvenGlowingLED<PIN, NUM_LEDS>
 * using L = LinearLED<PIN, NUM_LEDS>
 * 
 * PIN denotes the actual pin you used to connect the strip with the controller
 * and NUM_LEDS is the number of leds the strip is containing
 * 
 * ATTENTION:
 * Don't use Pin 0, 2, 4, 12 - 15 and 25 - 27 for your microphone! 
 * When WIFI is used, the pins below are used in a different fashion
 * https://docs.espressif.com/projects/esp-idf/en/latest/api-reference/peripherals/adc.html
 */

using LED1 = LinearLED<19, 60>;

/* Finally add all your LED stripse t the manager.
 * 
 * using LEDManager = LedStripManager<LED1, M, E, L>;
 */

using LEDManager = LedStripManager<LED1>;
LEDManager led_manager;

/**
 * Define the microphone here. The first template parameter gives the PIN, the second one the sample rate
 * If no microphone is used, then change the line to using Mic = Microphone<>;
 * 
 * ATTENTION:
 * The sample rate have to be a power of two (2^x -> 1,2,4,8,16,32,64,128,256). 
 * And for some reason not bigger than 256 !
 * (probably something from the library, we'll figure it out someday)
 * 
 * ATTENTION:
 * Don't use Pin 0, 2, 4, 12 - 15 and 25 - 27 for your microphone! 
 * When WIFI is used, the pins below are used in a different fashion
 * https://docs.espressif.com/projects/esp-idf/en/latest/api-reference/peripherals/adc.html
 * 
 * ALSO: 
 * You need to adopt some parameters inside FFT.h when the changing the sample size from 64, cause the 
 * output frequencies change from FFT and therefore the computed frequency range
 */

using Mic = Microphone<34, 64>;
Mic m;

/**
 * ==============================================
 * FROM HERE ON PLEASE DON'T ALTER THE CODE !!!!!
 * 
 *       except you know what you do :)
 * ==============================================
 */

std::vector<CHSV> msg_rcv_container;
std::vector<CHSV> msg_snd_container;

using FFTM = FFTManager<Mic>;
FFTM fft_manager(m, msg_snd_container);

/**
 * This here will set up the colors for the LED stripes
 * 
 * Depending on having an own mic and/or the signals coming from the
 * other participants in the network
 */
using LEDColorManager = LedColorManager<FFTM, LEDManager>;
LEDColorManager led_color_manager(fft_manager, led_manager, msg_rcv_container);

TaskHandle_t Task1;
TaskHandle_t Task2;

IPAddress local_ip;
IPAddress local_broadcast;

WiFiUDP udp;
const int udpPort = 666;

//this function will controll the wifi, send and reciev messages from the others
void WifiControllerFunction(void *pvParameters)
{
  // initialize the WIFI controller
  WiFi.begin(networkName, networkPswd);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
  }

  Serial.println("WIFI set up ....");

  // set up broadcast address of WIFI
  local_ip = WiFi.localIP();
  local_broadcast = local_ip;
  local_broadcast[3] = 255;

  // set up the udp connection
  udp.begin(udpPort);

  // loop
  while (1)
  {
    // first read ....
    int packetSize = udp.parsePacket();
    if (packetSize)
    {
      // read the packet into packetBufffer
      uint8_t r[3];
      udp.read(r, 3);

      CHSV res;
      res.h = r[0];
      res.s = r[1];
      res.v = r[2];

      // put it somewhere, where others can read it ;)
      msg_rcv_container.push_back(res);
    }

    // ... then write
    if (msg_snd_container.size())
    {
      // there are messages to send.... send them
      for (const auto &elem : msg_snd_container)
      {
        uint8_t msg[3];

        msg[0] = elem.h;
        msg[1] = elem.s;
        msg[2] = elem.v;

        udp.beginPacket(local_broadcast, udpPort);
        udp.write(msg, 3);
        udp.endPacket();
      }

      msg_snd_container.clear();
    }
    // this delay is important, otherwise the watchdog
    // triggers and reboots the controller
    delay(5);
  }
}

// this function will controll all the parts 
// connected to the controller e.g. mics, strips, compute FFT ....
void LedControllerFunction(void *pvParameters)
{
  // initialize the on-board setup
  taskManager.scheduleFixedRate(10, &m, TIME_MICROS);
  taskManager.scheduleFixedRate(500, &fft_manager, TIME_MICROS);
  taskManager.scheduleFixedRate(500, &led_manager, TIME_MICROS);
  taskManager.scheduleFixedRate(500, &led_color_manager, TIME_MICROS);

  // loop
  while (1)
  {
    taskManager.runLoop();
    FastLED.show();
  }
}

void setup()
{
  Serial.begin(115200);

  xTaskCreatePinnedToCore(
      WifiControllerFunction, /* Task function. */
      "WIFI Controller",      /* name of task. */
      10000,                  /* Stack size of task */
      NULL,                   /* parameter of the task */
      1,                      /* priority of the task */
      &Task1,                 /* Task handle to keep track of created task */
      0);                     /* pin task to core 0 */
  delay(500);

  xTaskCreatePinnedToCore(
      LedControllerFunction, /* Task function. */
      "Led Controller",      /* name of task. */
      10000,                 /* Stack size of task */
      NULL,                  /* parameter of the task */
      1,                     /* priority of the task */
      &Task2,                /* Task handle to keep track of created task */
      1);                    /* pin task to core 1 */
  delay(500);
}

void loop()
{
}