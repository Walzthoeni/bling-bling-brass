# Welcome to Bling-Bling-Brass

this project has been started at the Strafiato Festival in Innsbruck. A bunch of brass instrument players wanted to create a nice setup for all bands over the world, so the are able to illuminate their played music.

The idea is to use a microcontroller to perform a FFT (Fast Fourier Transformation) to get a peak frequency. This peak is then used to alter the output of the connected LED strips.

## How is it working?

### Sound analysis

Givan a microphone, which is defined in the `main.cpp` file, is given by a certain input **PIN** and a number of **SAMPLES**, which are stored over time.
```cpp
using Mic = Microphone<PIN, SAMPLES>;
```

To change the frequency, at which the signals are recorded, the line 
```cpp
taskManager.scheduleFixedRate(10, &m, TIME_MICROS);
```

The Fourier transformation is triggered every 0.5ms. This can be changes, by altering the time in the line 
```cpp
taskManager.scheduleFixedRate(500, &fft_manager, TIME_MICROS);
``` 

The computed result is then given to the ``LEDManager``, which distributes it to all LED strips, which are defines in 
```cpp
using LED1 = LinearLED<19, 60>;
using LEDManager = LedStripManager<LED1>;
```

### Distribution of of signals
- toDo

## What do you have to do to get it work for your band?

First of all: NICE !!! Your band will look surely awesome with illumination. But it is propably a little bit of work to get there. But I can assure, that it will be worth it and in the worst case scenario you learn some nice skills.

### Personal requirements

There are some pretty simple skills you should have to make use of this project. Soldering, some basics stuff from electronics and how to use a PC. Assuming you know how to use a PC, if you want to learn how to solder here is a nice introduction on [Youtube](https://www.youtube.com/watch?v=Qps9woUGkvI "Youtube")  , for basic electrinics i recommend the tutorials given by [Adafruit](https://learn.adafruit.com/ "Adafruit") or [HacksterIO](https://www.hackster.io/ "HacksterIO").

If you havn't been soldering, and/or have to do anything with electronics and are also not intend to do so, then it shouldn't be a problem. There is normally in each bigger city a hackerspace or a fab-lab where there are people which can help you out! Just google them.

### Hardeware requirements

You will need some stuff to get this thing running. But the hardware requirements are pretty cheap! All you need is a ESP32 (it will probably also work with other SBCs, but I tested it only for the ESP32 - in particular the Espressif ESP32 Dev Kit), a 3 or 5-pin microphone and an LED strip with an WS2812b or WS2811 controller (this it rather important). You will find these products either by some fast deliverer e.g. reichelt.com or if you intend to assamble the whole band I would recommend a chinese deliverer (6 weeks of waiting, but waaaaaaay cheaper ... and one way or the other it has the exactly same delivery way :sob: :sob:).

When the part are here a little assembling has to be done. We need to connect the microphone with 3 and the LED strip with either one or three cables to the microcontroller.

#### Microphone harware setup

The microphone has normally a plus, a ground and a output pin. Your microcontroller should have a ground and a constant 3V or 5V pin available. The output of the mic should be connected to an analog input of the controller. Remember the pin number.

*Add picture here*

#### LED strip harware setup

The LED stip has typically 3 pins. Two for power supply and one for the data input. The power supply should match the voltage requirements of the LED strip. You can connect also multiple LED strips to your controller with various amounts of LEDs on it. Also remember the pin numbers plus the size of the LED strip for later.

*Add picture here*

### What to install

First of all you should have an installation of VisualStudio Code. If not, just visit their website, [download](https://code.visualstudio.com/download "VS Code") and install it.

One plugin needed is PlatformIO. You can install it by clicking on the extension icon on the left taskbar, look it up with the search function and click on the **Install** button.

*Add picture here*

Note: This plugin supports the project by installing libaries and providing additional code analysis support (... better than ArduinoIDE :no_mouth: in most of the cases). Here it will download the FastLED and IoAbstraction lib. These are used to communicate with the LED stripes and as event handler.

### What do you need to change?

First you download this project as zip file and extract it on your PC. After that, open this folder with VisualStudio Code (if you want to use an other IDE or build system then you most likely move main.cpp in the src folder into the mainfolder and rename it to <mainfolder-name>\<mainfolder-name>.ino ... also let me know if we should make this a cmake project). 

To create the setup for a single musician, you need first make some hardware work. 

Open the ``main.cpp`` file in the src folder inside code and you just have to focus on a few lines. Inside you have to adapt the pins, number of leds per strip and the sample size for the microphone. 

To tell the application, that there is and LED strip connected, just add a line like ``using LEDName = LEDBehavior<PIN, SIZE>;`` in front of the LEDManager. You can change ``LEDName`` to any name you want, set the ``PIN`` and ``SIZE`` of the one of your LED strip and choose the ``LEDBehavior``-model from one of [these](#-LED-strip-behavior-models).

***

### LED strip behavior models

Here is a short presentation of the usable behavior models for the LED stripes:

#### LinearLED<PIN, NUM, DIR>

The LinearLED will take the provided color and shift it each turn in a certain direction. The speed of the turns can be alteres by changing the first parameter of ``taskManager.scheduleFixedRate(500, &led_manager, TIME_MICROS);``.

This model takes beside PIN and NUM and a third parmeter ``using Led1 = LinearLED<18, 60, false>;``. ``PIN`` being ``18``, ``NUM`` displaying the numbers of LEDs as ``60`` and a so called *direction* parameter, which is ``false``. The direction is the way the given values are shifted. 

#### MiddleToOutsideLED<PIN, NUM>

This behavior model is doing principally the same as the [LinearLED](#-LinearLED<PIN,-NUM,-DIR>), just twice from the middle of the strip to the outside. A new value will be inserted into the middle and the shifted left and right to the outside.

#### EvenGlowingLED<PIN, NUM>

This is probably the most trivial model. It just takes a given value and displays it on all LEDs the same time.

*** 

Installing a microphone is also not that hard: just change the line ``using Mic = Microphone<PIN, SAMPLESIZE>;`` with the ``PIN`` you have chosen during the hardware assemblation and the ``SAMPLESIZE`` you desire. If you are not intending to use a microhone please change the line to ``using Mic = Microphone<>;``.

*Note*: if no microphone is connected and also no input from outside is incoming, then the LED strip might not blink at all ....

*Attention*: ``SAMPLESIZE`` should be a power of 2 and not greater than 256 (havn't found the reason why yet, but my controller then crashes).

### Congrats

And with this you are done! You should just have to power your microcontroller, led and microphone and the application will do the rest for you ;)

## You have questions?

Write a mail.

## You have ideas?

You have ideas of how the project can be improved? The let us know! Ideas for new behavior models or ideas about how different communication models in between the single systems are welcome!! If you also have some knowledge in programming and/or electronics and want to do something youself, then please jsut go on with reading ;)
 
## You want to participate?

Whoop, whoop! This is great. Please just clone the project as you want, if you need permissions, then just ask for them. If you see any flaws or other things, then please just write an issues :)