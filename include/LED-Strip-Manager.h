#pragma once

#include <iostream>
#include <vector>
#include <FastLED.h>
#include <Wire.h>
#include "LED-Stripes.h"

namespace Detail
{

struct fTor1
{
    template <typename T>
    void operator()(T &elem)
    {
        elem.operator()();
        return;
    }
};

struct fTor2
{
    template <typename T, typename U>
    void operator()(T &elem, const U &v)
    {
        elem.setValue(v);
        return;
    }
};

} // end namespace Detail

template <typename... LEDS>
struct LedStripContainer;

template <template <std::size_t, std::size_t> class L, std::size_t P, std::size_t N, typename... Rest>
struct LedStripContainer<L<P, N>, Rest...>
{
    static const std::size_t Pin = P;
    static const std::size_t Num = N;

    using Led = L<P, N>;
    Led l;

    using Nested = LedStripContainer<Rest...>;
    Nested n;

    template <typename F, typename... Args>
    void forAll( F &f, Args... a)
    {
        f(l, a...);
        n.forAll(f, a...);
    }
};

template <template <std::size_t, std::size_t, bool> class L, std::size_t P, std::size_t N, bool D, typename... Rest>
struct LedStripContainer<L<P, N, D>, Rest...>
{
    static const std::size_t Pin = P;
    static const std::size_t Num = N;
    static const bool Dir = D;

    using Led = L<P, N, D>;
    Led l;

    using Nested = LedStripContainer<Rest...>;
    Nested n;

    template <typename F, typename... Args>
    void forAll( F &f, Args... a)
    {
        f(l, a...);
        n.forAll(f, a...);
    }
};

template <>
struct LedStripContainer<>
{
    template <typename F, typename... Args>
    void forAll( F &f, Args... a) {}
};

template <typename... LEDS>
struct LedStripManager : public Executable
{
    LedStripContainer<LEDS...> container;

    void exec()
    {
        Detail::fTor1 f;
        container.forAll(f);
    }

    void updateColor(const CHSV &v)
    {
        Detail::fTor2 f;
        container.forAll(f, v);
    }
};