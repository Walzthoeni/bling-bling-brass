#pragma once

#include <type_traits>

/*
 * Helper utils for BodyParts template
 */

namespace Utils
{

template <typename Lookup, typename Fields>
struct template_lookup;

template <typename Lookup, typename Fields>
struct template_lookup_pos;

template <typename Lookup, typename... Fields>
struct template_lookup_pos_c;

template <std::size_t Lookup, std::size_t... Fields>
struct template_lookup_pos_n;

template <typename... Types>
struct template_iterator;

template <std::size_t... Types>
struct template_iterator_n;

template <typename T>
using plain_type = typename std::remove_cv<typename std::remove_reference<T>::type>::type;

template <std::size_t First, std::size_t... Rest>
struct template_iterator_n<First, Rest...>
{
    template <typename Body>
    void operator()(const Body &body)
    {
        body(First);
        template_iterator_n<Rest...>()(body);
    }
};

template <>
struct template_iterator_n<>
{
    template <typename Body>
    void operator()(const Body &body) {}
};

template <typename First, typename... Rest>
struct template_iterator<First, Rest...>
{
    template <typename Body>
    void operator()(const Body &body)
    {
        body(First());
        template_iterator<Rest...>()(body);
    }
};

template <>
struct template_iterator<>
{
    template <typename Body>
    void operator()(const Body &body) {}
};

template <typename... Fields>
struct template_container
{
    enum
    {
        size = sizeof...(Fields)
    };

    template <typename Lookup>
    constexpr std::size_t get()
    {
        return template_lookup_pos_c<Lookup, Fields...>::value;
    }

    template <typename Body>
    void forEachType(const Body &body)
    {
        template_iterator<Fields...>()(body);
    }
};

template <std::size_t... Fields>
struct template_container_num
{
    enum
    {
        size = sizeof...(Fields)
    };

    template <std::size_t Lookup>
    constexpr std::size_t get()
    {
        return template_lookup_pos_n<Lookup, Fields...>::value;
    }

    template <typename Body>
    void forEachType(const Body &body)
    {
        template_iterator_n<Fields...>()(body);
    }
};

template <std::size_t Lookup, std::size_t First, std::size_t... Rest>
struct template_lookup_pos_n<Lookup, First, Rest...>
{
    enum
    {
        value = 1 + template_lookup_pos_n<Lookup, Rest...>::value
    };
};

template <std::size_t Lookup, std::size_t... Rest>
struct template_lookup_pos_n<Lookup, Lookup, Rest...>
{
    enum
    {
        value = 0
    };
};

template <std::size_t Lookup, std::size_t... Rest>
struct template_lookup_pos_n
{
    static_assert(sizeof...(Rest) != 0, "There is no such type!");
    enum
    {
        value = 0
    };
};

template <typename Lookup, typename First, typename... Rest>
struct template_lookup_pos_c<Lookup, First, Rest...>
{
    enum
    {
        value = 1 + template_lookup_pos_c<Lookup, Rest...>::value
    };
};

template <typename Lookup, typename... Rest>
struct template_lookup_pos_c<Lookup, Lookup, Rest...>
{
    enum
    {
        value = 0
    };
};

template <typename Lookup, typename... Rest>
struct template_lookup_pos_c
{
    static_assert(sizeof...(Rest) != 0, "There is no such type!");
    enum
    {
        value = 0
    };
};

template <typename Lookup, template <typename...> typename Container, typename First, typename... Rest>
struct template_lookup_pos<Lookup, Container<First, Rest...>>
{
    enum
    {
        value = 1 + template_lookup_pos<Lookup, Container<Rest...>>::value
    };
};

template <typename Lookup, template <typename...> typename Container, typename... Rest>
struct template_lookup_pos<Lookup, Container<Lookup, Rest...>>
{
    enum
    {
        value = 0
    };
};

template <typename Lookup, template <typename...> typename Container, typename... Rest>
struct template_lookup_pos<Lookup, Container<Rest...>>
{
    static_assert(sizeof...(Rest) != 0, "There is no such type!");
    enum
    {
        value = 0
    };
};

template <typename Lookup, template <typename...> typename Container, typename... Fields>
struct template_lookup<Lookup, Container<Fields...>>
{
    enum
    {
        pos = template_lookup_pos<Lookup, Container<Fields...>>::value
    };
};

template <typename...>
struct is_same_t;

template <typename T>
struct is_same_t<T>
{
    static constexpr bool value = true;
};

template <typename T, typename U, typename... V>
struct is_same_t<T, U, V...>
{
    static constexpr bool value = std::is_same<T, U>::value && is_same_t<T, V...>::value;
};

template <typename...>
struct is_one_of_t;

template <typename F>
struct is_one_of_t<F>
{
    static constexpr bool value = false;
};

template <typename F, typename S, typename... T>
struct is_one_of_t<F, S, T...>
{
    static constexpr bool value = std::is_same<F, S>::value || is_one_of_t<F, T...>::value;
};

template <std::size_t...>
struct is_one_of_n;

template <std::size_t F>
struct is_one_of_n<F>
{
    static constexpr bool value = false;
};

template <std::size_t F, std::size_t S, std::size_t... T>
struct is_one_of_n<F, S, T...>
{
    static constexpr bool value = (F == S) || is_one_of_n<F, T...>::value;
};

template <typename...>
struct is_unique_t;

template <>
struct is_unique_t<>
{
    static constexpr bool value = true;
};

template <typename F, typename... T>
struct is_unique_t<F, T...>
{
    static constexpr bool value = is_unique_t<T...>::value && !is_one_of_t<F, T...>::value;
};

template <std::size_t...>
struct is_unique_n;

template <>
struct is_unique_n<>
{
    static constexpr bool value = true;
};

template <std::size_t F, std::size_t... T>
struct is_unique_n<F, T...>
{
    static constexpr bool value = is_unique_n<T...>::value && !is_one_of_n<F, T...>::value;
};

} // end namespace Utils
