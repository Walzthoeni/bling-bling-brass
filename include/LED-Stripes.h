#pragma once 

#include <cstdio>
#include <cmath>
#include <vector>
#include <array>
 
#include <FastLED.h>
#include <IoAbstraction.h>

// This is the hardware definition of a LED strip with all its content
template <std::size_t PIN, std::size_t NUM>
struct LEDStrip
{
    static const std::size_t Pin = PIN;
    static const std::size_t Num = NUM;

    unsigned refresh_rate = 200;

    CRGB leds[NUM];

    LEDStrip()
    {
        // initialize all single leds
        for (auto i = 0; i < NUM; ++i)
        {
            leds[i] = (CHSV(0, 0, 0));
        }
        
        // initialize output from library
        FastLED.addLeds<NEOPIXEL, PIN>(leds, NUM);
    }
};

// these are behaviour models for the LED strip
template <std::size_t PIN, std::size_t NUM, bool DIR = true>
struct LinearLED : public LEDStrip<PIN, NUM>
{
    static const std::size_t num = NUM;
    static const bool direction = DIR;
    std::size_t iterator = 0;
    std::size_t last_fill = 0;
    std::array<CHSV, NUM> values;

    void setValue(const CHSV &val)
    {
        values[iterator] = val;
    }

    void setValue(const unsigned short &h, const unsigned short &s, const unsigned short &v)
    {
        setValue(CHSV(h, s, v));
    }

    void operator()()
    {
        if(values[iterator] != CHSV(0,0,0))
        {
            last_fill = 0;
        }
        else
        {
            last_fill++;
            if (last_fill > num)
            {
                iterator = 0;
            }
        }

        if (direction)
        {
            for (auto i = 0u; i < this->Num; ++i)
            {
                this->leds[(i + iterator) % this->Num] = values[i];
            }
        }
        else
        {
            for (auto i = 0u; i < this->Num; ++i)
            {
                const auto index = this->Num - (i + iterator) < 0 ? 2 * this->Num - (i + iterator) : this->Num - (i + iterator);
                this->leds[index] = values[index];
            }
        }

        //shift the iterator
        iterator = (iterator + 1) % this->Num;
    }
};

template <std::size_t PIN, std::size_t NUM>
struct MiddleToOutsideLED : public LEDStrip<PIN, NUM>
{
    std::size_t iterator = 0;
    std::array<CHSV, NUM> values;    

    void setValue(const CHSV &val)
    {
        values[iterator] = val;
    }

    void setValue(const unsigned short &h, const unsigned short &s, const unsigned short &v)
    {
        values[iterator] = CHSV(h, s, v);
    }

    void operator()()
    {
        for (auto i = 0u; i < this->Num / 2; ++i)
        {
            const auto left = std::max(0u, (this->Num / 2) - i);
            const auto right = std::min(255u, (this->Num / 2) + i);
            this->leds[left] = values[i];
            this->leds[right] = values[i];
        }

        //shift the iterator
        iterator = (iterator + 1) % (this->Num / 2);
    }
};

template <std::size_t PIN, std::size_t NUM>
struct EvenGlowingLED : public LEDStrip<PIN, NUM>
{
    CHSV value = CHSV(0, 0, 0);

    void setValue(const CHSV &val)
    {
        if (val == CHSV(0,0,0) && value.v >= 10)
        {
            value.v -= 10;
        }
        else
        {
            value = val;
        }        
    }

    void setValue(const unsigned short &h, const unsigned short &s, const unsigned short &v)
    {
        setValue(CHSV(h, s, v));
    }

    void operator()()
    {
        for (auto i = 0u; i < this->Num; ++i)
        {
            this->leds[i] = value;
        }
    }
};