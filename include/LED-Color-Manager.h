#include <FFT.h>

#include <string>

template <typename F, typename L>
struct LedColorManager;

template <typename Mic, typename... LEDS>
struct LedColorManager<FFTManager<Mic>, LedStripManager<LEDS...>> : public Executable
{
    FFTManager<Mic> &f;
    LedStripManager<LEDS...> &l;
    std::vector<CHSV> &msg_container;

    CHSV old_val;

    LedColorManager(FFTManager<Mic> &_f, LedStripManager<LEDS...> &_l, std::vector<CHSV> &m)
        : f(_f), l(_l), msg_container(m) {}

    /**
     * This function here should get the single values from the neighbors and/or own FFT,
     * combine them in a nice fashion and finally feed the LedStripManager with it
     */
    void exec()
    {
        // collect all data
        const auto own_val = f.getValue();

        // compute resulting color

        CHSV res(0, 0, 0);

        if (own_val != CHSV(0, 0, 0))
        {
            res = own_val;

            // clear msg_container for this turn
            msg_container.clear();
        }
        else if (msg_container.size())
        {
            double h = 0, s = 0, v = 0;

            for (auto &e : msg_container)
            {
                h += e.h;
                s += e.s;
                v += e.v;
            }

            h /= static_cast<double>(msg_container.size());
            s /= static_cast<double>(msg_container.size());
            v /= static_cast<double>(msg_container.size());

            res = CHSV(h, s, v);

            msg_container.clear();

        }
        else
        {
            res = old_val;
            if(res.v > 10)
            {
                res.v -= 10;
            }
            else 
            {
                res.v = 0;
            }
        }

        // set color
        old_val = res;
        // Serial.println(res.h);
        l.updateColor(res);
    }
};