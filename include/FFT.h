#pragma once

#include <array>
#include <memory>
#include <cstdlib>

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>

#include <FastLED.h>
#include <arduinoFFT.h>
#include <LED-Strip-Manager.h>

template <std::size_t S>
struct ComputeFFT
{
    static const std::size_t sample = S;
    const double signalFrequency = 1000;
    const double samplingFrequency = 5000;
    const uint8_t amplitude = 100;

    double vImag[sample];
    arduinoFFT FFT = arduinoFFT();

    CHSV operator()(double vReal[sample])
    {
        // Build raw data
        for (uint16_t i = 0; i < sample; i++)
        {
            vImag[i] = 0.0;
        }

        // Print the results of the simulated sampling according to time
        FFT.Windowing(vReal, sample, FFT_WIN_TYP_HAMMING, FFT_FORWARD);
        FFT.Compute(vReal, vImag, sample, FFT_FORWARD);
        FFT.ComplexToMagnitude(vReal, vImag, sample);

        // TODO : check out, if this would work...
        // double x = FFT.MajorPeak(vReal, sample, samplingFrequency);
        // Serial.println(x, 6);

        // compute index with highest amplitude
        double maxY = 0;
        uint16_t IndexOfMaxY = 0;
        for (uint16_t i = 1; i < ((sample >> 1) + 1); i++)
        {
            if ((vReal[i - 1] < vReal[i]) && (vReal[i] > vReal[i + 1]))
            {
                if (vReal[i] > maxY)
                {
                    maxY = vReal[i];
                    IndexOfMaxY = i;
                }
            }
        }

        // if the amplitude is too low, then handle it as noise and return black
        if (vReal[IndexOfMaxY] < 2000)
        {
            return CHSV(0, 255, 0);
        }

        // set the led to the right color
        const auto color_val = std::min(255.0, (IndexOfMaxY / 30.0) * 256);
        // Serial.println(color_val);

        // Serial.println(color_val);
        return CHSV(color_val, 255, 255);
    }
};

template <std::size_t PIN = 0, std::size_t SAMPLES = 0>
struct Microphone : public Executable
{
    static const std::size_t pin = PIN;
    static const std::size_t samples = SAMPLES;

    std::array<double, samples> measurements;
    std::size_t iterator;

    Microphone() : iterator(0)
    {
        measurements.fill(0.0);
        pinMode(pin, INPUT);
    }

    void measureSignal()
    {
        measurements[iterator] = analogRead(pin);
        iterator = (iterator + 1) % samples;
    }

    void exec()
    {
        measureSignal();
    }

    double getMeasement()
    {
        return analogRead(pin);
    }

    std::array<double, SAMPLES> getMeasurements() const
    {
        std::array<double, SAMPLES> res;

        for (auto i = 0u; i < samples; ++i)
        {
            res[i] = measurements[(i + iterator) % samples];
        }

        return res;
    }
};

template <typename T>
struct FFTManager;

template <std::size_t PIN, std::size_t SAMPLES>
struct FFTManager<Microphone<PIN, SAMPLES>> : public Executable
{

    static const std::size_t pin = PIN;
    static const std::size_t samples = SAMPLES;

    using Mic = Microphone<PIN, SAMPLES>;
    Mic &mic;

    std::vector<CHSV> &snd_container;

    CHSV value;

    FFTManager(Mic &m, std::vector<CHSV> &s) : mic(m), snd_container(s) {}

    void exec()
    {
        // get the microphone data
        auto mic_val = mic.getMeasurements();

        // compute FFT
        value = ComputeFFT<samples>()(mic_val.data());
        
        if (value != CHSV(0, 0, 0))
        {
            snd_container.push_back(value);
        }
    }

    const CHSV getValue() const
    {
        return value;
    }
};

/**
 * This is the no microphone option. 
 * This one wont measure anything and always return 0 as measured peak frequency
 */
template <std::size_t SAMPLES>
struct FFTManager<Microphone<0, SAMPLES>> : public Executable
{

    static const std::size_t pin = 0;
    static const std::size_t samples = SAMPLES;

    const CHSV value = CHSV(0, 0, 0);

    using Mic = Microphone<0, SAMPLES>;
    Mic &mic;

    FFTManager(Mic &m, std::vector<CHSV> &) : mic(m) {}

    void exec()
    {
    }

    const CHSV getValue() const
    {
        return value;
    }
};